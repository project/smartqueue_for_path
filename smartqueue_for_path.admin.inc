<?php
// $Id $



function smartqueue_for_path_settings($bundle = NULL) {
  if (is_null($bundle)) {
    return _smartqueue_for_path_bundle_summary();
  }
  return _smartqueue_for_path_path_summary($bundle);
}




function _smartqueue_for_path_bundle_summary() {
  $result = db_query('SELECT * FROM {smartqueue_for_path_bundles} ORDER BY name ASC');

  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      check_plain($row->name),
      implode('&nbsp;|&nbsp;', array(
        l(t('View'), 'admin/settings/smartqueue_for_path/'. $row->bid),
        l(t('Add Path'), 'admin/settings/smartqueue_for_path/'. $row->bid .'/add'),
        l(t('Edit'), 'admin/settings/smartqueue_for_path/'. $row->bid .'/edit'),
        l(t('Delete'), 'admin/settings/smartqueue_for_path/'. $row->bid .'/delete'),
      )),
    );
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('No path bundles - please !link', array(
          '!link' => l(t('create one'), 'admin/settings/smartqueue_for_path/add')
        )),
        'colspan' => 2,
      ),
    );
  }

  return theme('table', array(t('Bundle'), t('Ops')), $rows);
}


function _smartqueue_for_path_path_summary($bundle) {
  $result = db_query('SELECT p.*
                      FROM {smartqueue_for_path_bundle_paths} bp, {smartqueue_for_path_paths} p
                      WHERE bp.bid = %d AND bp.pid = p.pid
                      ORDER BY p.path ASC', $bundle->bid);

  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      '<code>'. check_plain($row->path) .'</code>',
      check_plain($row->description),
      implode('&nbsp;|&nbsp;', array(
        l(t('Edit'), 'admin/settings/smartqueue_for_path/'. $bundle->bid .'/'. $row->pid .'/edit'),
        l(t('Delete'), 'admin/settings/smartqueue_for_path/'. $bundle->bid .'/'. $row->pid .'/delete'),
      )),
    );
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('There are no paths in this bundles - please !link', array(
          '!link' => l(t('create some'), 'admin/settings/smartqueue_for_path/'. $bundle->bid .'/add')
        )),
        'colspan' => 3,
      ),
    );
  }

  return theme('table', array(t('Bundle'), t('Description'), t('Ops')), $rows);
}





function smartqueue_for_path_bundle_form(&$form_state, $bundle = NULL) {
  $form = array();

  if (isset($bundle)) {
    $form['#bundle'] = $bundle;
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Please enter the name of the path bundle'),
    '#size' => 20,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => isset($bundle->name) ? $bundle->name : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}



function smartqueue_for_path_bundle_form_submit($form, &$form_state) {
  if (isset($form['#bundle'])) {
    if ($form['#bundle']->name != $form_state['values']['name']) {
     db_query('UPDATE {smartqueue_for_path_bundles} SET name ="%s" WHERE bid = %d', $form_state['values']['name'], $form['#bundle']->bid);
    }
  }
  else {
    db_query('INSERT INTO {smartqueue_for_path_bundles} (name) VALUES ("%s")', $form_state['values']['name']);
  }
  $form_state['redirect'] = 'admin/settings/smartqueue_for_path';
}




function smartqueue_for_path_bundle_delete_confirm(&$form_state, $bundle) {
  $form = array();
  $form['#bundle'] = $bundle;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the bundle %title?', array('%title' => $bundle->name)),
    'admin/settings/smartqueue_for_path',
    t('Any paths associated with this bundle will be lost along with any associated smart-queues. This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}


function smartqueue_for_path_bundle_delete_confirm_submit($form, &$form_state) {
  $bundle = $form['#bundle'];

  db_query('DELETE FROM {smartqueue_for_path_bundles} WHERE bid = %d', $bundle->bid);
  db_query('DELETE FROM {smartqueue_for_path_bundle_paths} WHERE bid = %d', $bundle->bid);
  //TODO: DELETE ASSOCIATED PATHS

  $form_state['redirect'] = 'admin/settings/smartqueue_for_path';
}




function smartqueue_for_path_path_delete_confirm(&$form_state, $bundle, $path) {
  $form = array();
  $form['#bundle'] = $bundle;
  $form['#path'] = $path;

  return confirm_form(
    $form,
    t('Are you sure you want to delete the path <code>@path</code>?', array('@path' => $path->path)),
    'admin/settings/smartqueue_for_path/'. $bundle->bid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}


function smartqueue_for_path_path_delete_confirm_submit($form, &$form_state) {
  $bundle = $form['#bundle'];
  $path = $form['#path'];

  db_query('DELETE FROM {smartqueue_for_path_paths} WHERE pid = %d', $path->bid);
  db_query('DELETE FROM {smartqueue_for_path_bundle_paths} WHERE bid = %d AND pid = %d', $bundle->bid, $path->pid);

  $form_state['redirect'] = 'admin/settings/smartqueue_for_path/'. $bundle->bid;
}









function smartqueue_for_path_path_form($form_state, $bundle, $path = NULL) {
  $form = array();

  $form['#bundle'] = $bundle;
  if (isset($path)) {
    $form['#path'] = $path;
  }

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Enter the path'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => isset($path) ? $path->path : '',
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Optional short description for this path'),
    '#rows' => 2,
    '#default_value' => isset($path) ? $path->description : '',
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


function smartqueue_for_path_path_form_validate($form, &$form_state) {
  if ($form['path']['#default_value'] != $form_state['values']['path']) {
    $sql = 'SELECT p.pid
            FROM {smartqueue_for_path_paths} p, {smartqueue_for_path_bundle_paths} pb
            WHERE p.path = "%s" AND p.pid = pb.pid AND pb.bid = %d
            LIMIT 1';

    if (db_result(db_query($sql, $form_state['values']['path'], $form['#bundle']->bid))) {
      form_set_error('path', t('This path already exists in this bundle'));
    }
  }
}


function smartqueue_for_path_path_form_submit($form, &$form_state) {
  $bundle = $form['#bundle'];
  // Init fields & values
  $fields = array();
  $values = array();

  // Store the path
  $fields['path'] = '"%s"';
  $values['path'] = $form_state['values']['path'];

  // Store the description if it's set
  if (empty($form_state['values']['description'])) {
    $fields['description'] = 'NULL';
  }
  else {
    $fields['description'] = '"%s"';
    $values['description'] = $form_state['values']['description'];
  }

  if (isset($form['#path'])) {
    $values['pid'] = $form['#path']->pid;
    $fields = implode(',', array_map('_smartqueue_for_path_update_sql', array_keys($fields), array_values($fields)));

    db_query('UPDATE {smartqueue_for_path_paths} SET '. $fields  .' WHERE pid = %d', $values);
  }
  else {
    // Insert new path
    $placeholders = implode(',', array_values($fields));
    $fields = implode(',', array_keys($fields));

    db_query('INSERT INTO {smartqueue_for_path_paths} ('. $fields .') VALUES('. $placeholders .')', $values);

    $pid = db_last_insert_id('smartqueue_for_path_paths', 'pid');
    db_query('INSERT INTO {smartqueue_for_path_bundle_paths} (bid, pid) VALUES (%d, %d)', $bundle->bid, $pid);
  }

  $form_state['redirect'] = 'admin/settings/smartqueue_for_path/'. $bundle->bid;
}

function _smartqueue_for_path_update_sql($field, $placeholder) {
  return "{$field} = {$placeholder}";
}
